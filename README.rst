.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/GraphPolaris_DataTypeInference.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/GraphPolaris_DataTypeInference
    .. image:: https://readthedocs.org/projects/GraphPolaris_DataTypeInference/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://GraphPolaris_DataTypeInference.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/GraphPolaris_DataTypeInference/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/GraphPolaris_DataTypeInference
    .. image:: https://img.shields.io/pypi/v/GraphPolaris_DataTypeInference.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/GraphPolaris_DataTypeInference/
    .. image:: https://img.shields.io/conda/vn/conda-forge/GraphPolaris_DataTypeInference.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/GraphPolaris_DataTypeInference
    .. image:: https://pepy.tech/badge/GraphPolaris_DataTypeInference/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/GraphPolaris_DataTypeInference
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/GraphPolaris_DataTypeInference

    image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
        :alt: Project generated with PyScaffold
        :target: https://pyscaffold.org/
    image:: https://img.shields.io/badge/-Click-000000?logo=click
        :alt: Click for CLI generation
        :target: https://click.palletsprojects.com/

|


==============================
GraphPolaris_DataTypeInference
==============================


    """
    This project is called GraphPolaris_DataTypeInference. It aims to provide a solution for inferring data types in a graph-based data structure. The code in this file is responsible for adding a short description to the project. 
    """


SQLite
------
The project utilizes SQLite, a lightweight and widely-used relational database management system. SQLite provides a simple and efficient way to store and retrieve data, making it suitable for various applications.

databasecreator.py
------------------
The `databasecreator.py` module is responsible for creating and managing the SQLite database used by the project. It provides functions to create tables, insert data, and perform other database operations.

main_cli
--------
The `main_cli` module is the entry point of the command-line interface (CLI) for the project. It handles user input, executes commands, and interacts with other modules to perform various tasks.

classifier_trainer.py
---------------------
The `classifier_trainer.py` module is responsible for training a machine learning classifier using the data stored in the SQLite database. It uses various algorithms and techniques to analyze the data and generate a model that can be used for classification tasks.

Setup
-----

To set up the project using PyScaffold, follow these steps:

1. Install PyScaffold:

    ```bash
    pip install pyscaffold
    ```

2. Navigate to the project directory:

    ```bash
    cd GraphPolaris_DataTypeInference
    ```

3. Create a virtual environment (optional but recommended):

    ```bash
    python -m venv venv
    ```

4. Activate the virtual environment:

    - On Windows:

      ```bash
      venv\Scripts\activate
      ```

    - On macOS and Linux:

      ```bash
      source venv/bin/activate
      ```

5. Install the project dependencies:

    ```bash
    pip install -e .
    ```


6. Start the command-line interface:

    ```bash
    python src\graphpolaris_datatypeinference\main_cli.py
    ```

That's it! You can now use the command-line interface to interact with the GraphPolaris_DataTypeInference project.

For more details and usage information on PyScaffold, refer to the [PyScaffold documentation](https://pyscaffold.org/en/stable/usage.html).

.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.5. For details and usage
information on PyScaffold see https://pyscaffold.org/.
