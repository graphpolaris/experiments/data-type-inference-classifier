import pickle
import sqlite3

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report

from sklearn.linear_model import LogisticRegression, PassiveAggressiveClassifier, Perceptron, SGDClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import MultinomialNB

import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, HashingVectorizer



from constants import TABLE_NAME

from graphpolaris_datatypeinference import __version__

__author__ = "Michael Behrisch"
__copyright__ = "Michael Behrisch"
__license__ = "MIT"



import sqlite3
import numpy as np

def load_data(url):
    # Connect to the SQLite database
    conn = sqlite3.connect(url)
    cursor = conn.cursor()

    # Fetch the attribute values and decision variable from the table
    cursor.execute(f'SELECT attribute_value, data_type FROM {TABLE_NAME}')
    data = cursor.fetchall()
    data = np.array(data)
    attribute_values = data[:, 0]
    decision_variable = data[:, 1]

    # Close the database connection
    conn.close()

    return attribute_values, decision_variable


def vectorize_input(attribute_values, decision_variable):
    # Convert text data to numerical feature vectors
    # vectorizer = CountVectorizer()
    vectorizer = HashingVectorizer(n_features=2**5, alternate_sign=False)
    attribute_values_vectorized = vectorizer.fit_transform(attribute_values)

    # Split the data into training and testing sets
    x_train, x_test, y_train, y_test = train_test_split(attribute_values_vectorized, decision_variable, test_size=0.2, random_state=42)

    return x_train, x_test, y_train, y_test, attribute_values_vectorized


def load_classifier(filename: str):
    with open(filename, 'rb') as f:
        classifier = pickle.load(f)
        return classifier

def train_classifier(url: str):
    attribute_values, decision_variable = load_data(url)
    x_train, x_test, y_train, y_test, attribute_values_vectorized = vectorize_input(attribute_values, decision_variable)

    # List of classifiers
    classifiers = [
        MultinomialNB(), 
        RandomForestClassifier(),
        SVC(),
        LogisticRegression(),
        GaussianNB(),
        KNeighborsClassifier(),
        MLPClassifier(),
        DecisionTreeClassifier(),
        AdaBoostClassifier(),
        QuadraticDiscriminantAnalysis(),
        GaussianProcessClassifier(),
        PassiveAggressiveClassifier(),
        Perceptron(),
        SGDClassifier(),
    ]

    # Variables to keep track of the best score and classifier
    best_score = 0
    best_classifier = None

    for classifier in classifiers:
        # Train the classifier
        # classifier.partial_fit(x_train.toarray(), y_train, classes=np.unique(decision_variable))
        classifier.fit(x_train.toarray(), y_train)

        # Make predictions on the test set
        y_pred = classifier.predict(x_test.toarray())

        # Evaluate the classifier's performance on the test set
        print(f"\n{classifier.__class__.__name__} Performance:")
        print(classification_report(y_test, y_pred))

        # # Calculate the F1 score
        # score = f1_score(y_test, y_pred, average='macro')

        # Perform k-fold cross-validation
        scores = cross_val_score(classifier, attribute_values_vectorized.toarray(), decision_variable, cv=5, scoring='f1_macro')
        print(f"Cross-Validation Scores: {scores}")
        print(f"Average Cross-Validation Score: {scores.mean():.3f}")
        
        # If this classifier's score is better than the current best score, update the best score and classifier
        if scores.mean() > best_score:
            best_score = scores.mean()
            best_classifier = classifier
            
            # Save the best classifier to a file
            with open('classifier.pkl', 'wb') as f:
                pickle.dump(best_classifier, f)
                print("Classifier saved to classifier.pkl")

    # After the loop, best_classifier is the classifier with the best F1 score
    print(f"\nBest Classifier: {best_classifier.__class__.__name__}")
    print(f"Best Score: {best_score:.3f}")

    # with open('classifier.pkl', 'wb') as f:
    #     pickle.dump(best_classifier, f)

    # validate classifier working
    savedClassifier = load_classifier('classifier.pkl')
    y_pred = savedClassifier.predict(x_test.toarray())

    # Evaluate the classifier's performance on the test set
    print(f"\n{savedClassifier.__class__.__name__} Saved Performance:")
    print(classification_report(y_test, y_pred))


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m graphpolaris_datatypeinference.databasecreator 
    #
    train_classifier('filetypeinference.db')
