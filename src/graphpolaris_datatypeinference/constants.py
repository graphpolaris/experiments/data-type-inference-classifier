TABLE_NAME = "data_type_inference"

INPUT_DATA_TYPES = ["numerical", "date", "time", "datetime", "boolean", "categorical", "categorical_licenseplate", "spatial_address", "spatial_geo"]

LOCALES = ['de_DE', 'en_US', 'nl_NL', 'fr_FR', 'en_GB', 'it_IT']