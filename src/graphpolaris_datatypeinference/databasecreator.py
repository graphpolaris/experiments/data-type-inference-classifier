"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
``[options.entry_points]`` section in ``setup.cfg``::

    console_scripts =
         fibonacci = graphpolaris_datatypeinference.skeleton:run

Then run ``pip install .`` (or ``pip install -e .`` for editable mode)
which will install the command ``fibonacci`` inside your current environment.

Besides console scripts, the header (i.e. until ``_logger``...) of this file can
also be used as template for Python modules.

Note:
    This file can be renamed depending on your needs or safely removed if not needed.

References:
    - https://setuptools.pypa.io/en/latest/userguide/entry_point.html
    - https://pip.pypa.io/en/stable/reference/pip_install
"""

import argparse
import logging
import sys
import sqlite3
from sqlite3 import Error
import math
from faker import Faker

from graphpolaris_datatypeinference import __version__

__author__ = "Michael Behrisch"
__copyright__ = "Michael Behrisch"
__license__ = "MIT"


_logger = logging.getLogger(__name__)


# ---- Python API ----
# The functions defined in this section can be imported by users in their
# Python scripts/interactive interpreter, e.g. via
# `from graphpolaris_datatypeinference.skeleton import fib`,
# when using this Python module as a library.

from constants import TABLE_NAME, LOCALES

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None;
    try:
        conn = sqlite3.connect(db_file)  # it will create a database if it doesn't exist
        print(sqlite3.version)
        return conn
    except Error as e:
        print(e)
        return None

def create_table(conn):
    """ create a table in the database """
    try:
        c = conn.cursor()
        c.execute(f'''CREATE TABLE IF NOT EXISTS {TABLE_NAME}
                     (id INTEGER PRIMARY KEY, 
                     attribute_name TEXT NOT NULL, 
                     attribute_value TEXT NOT NULL, 
                     data_type TEXT NOT NULL)''')
        
        c.execute(f"CREATE UNIQUE INDEX idx_unique_data_type_inference ON {TABLE_NAME} (attribute_name)")

        conn.commit()
    except Error as e:
        print(e)



def add_numerical_data(connection, fake = Faker(), countrycode = 'US', n = 1000):
    try:
        c = connection.cursor()
        for i in range(n):
            random_number = fake.random_number(fix_len=False)
            c.execute(f"INSERT OR IGNORE INTO {TABLE_NAME} (attribute_name, attribute_value, data_type) VALUES ('numerical_" + str(i) + "', '" + str(random_number) + "', 'numerical')")
            connection.commit()
    except Error as e:
        print(e)

def add_categorical_data(connection, fake = Faker(), countrycode = 'US', n = 1000):
    try:
        c = connection.cursor()
        for i in range(n):
            categorical_value = fake.company()
            c.execute(f"INSERT OR IGNORE INTO {TABLE_NAME} (attribute_name, attribute_value, data_type) VALUES ('categorical_" + countrycode + "_" + str(i) + "', '" + str(categorical_value) + "', 'categorical')")
            connection.commit()
    except Error as e:
        print(e)

def add_categorical_licenseplate_data(connection, fake = Faker(), countrycode = 'US', n = 1000):
    try:
        c = connection.cursor()
        for i in range(n):
            categorical_value = fake.license_plate()
            c.execute(f"INSERT OR IGNORE INTO {TABLE_NAME} (attribute_name, attribute_value, data_type) VALUES ('categorical_licenseplate_" + countrycode + "_" + str(i) + "', '" + str(categorical_value) + "', 'categorical')")
            connection.commit()
    except Error as e:
        print(e)

def add_spatial_data(connection, fake = Faker(), countrycode = 'US', n = 1000):
    type = 'spatial'
    try:
        c = connection.cursor()
        for i in range(n):
            value = fake.address()
            c.execute(f"INSERT OR IGNORE INTO {TABLE_NAME} (attribute_name, attribute_value, data_type) VALUES ('"+type+"_address_" + countrycode + "_" + str(i) + "', '" + str(value) + "', '"+type+"')")
            connection.commit()
    except Error as e:
        print(e)


def add_geo_data(connection, fake = Faker(), countrycode = 'en_US', n = 1000):
    type = 'spatial'
    try:
        c = connection.cursor()
        for i in range(n):
            value = fake.local_latlng(coords_only=True)
            fString = f"INSERT OR IGNORE INTO {TABLE_NAME} (attribute_name, attribute_value, data_type) VALUES ('"+type+"_geo_" + countrycode + "_" + str(i) + "', '" + str(value[0]) + ", " + str(value[1]) + "', '"+type+"')"
            c.execute(fString)
            connection.commit()
    except Error as e:
        print(e)

def add_temporal_data(connection, fake = Faker(), countrycode = 'US', n = 1000):
    type = 'temporal'
    try:
        c = connection.cursor()
        for i in range(n):
            value = fake.date_time_this_century()
            fString = f"INSERT OR IGNORE INTO {TABLE_NAME} (attribute_name, attribute_value, data_type) VALUES ('"+type+"_date_" + countrycode + "_" + str(i) + "', '" + str(value) + "', '"+type+"')"
            c.execute(fString)
            connection.commit()
    except Error as e:
        print(e)


def add_temporal_only_time_data(connection, fake = Faker(), countrycode = 'US', n = 1000):
    type = 'temporal'
    try:
        c = connection.cursor()
        for i in range(n):
            value = fake.time()
            fString = f"INSERT OR IGNORE INTO {TABLE_NAME} (attribute_name, attribute_value, data_type) VALUES ('"+type+"_time_" + countrycode + "_" + str(i) + "', '" + str(value) + "', '"+type+"')"
            c.execute(fString)
            connection.commit()
    except Error as e:
        print(e)


def add_temporal_only_date_data(connection, fake = Faker(), countrycode = 'US', n = 1000):
    type = 'temporal'
    try:
        c = connection.cursor()
        for i in range(n):
            value = fake.date_this_century()
            fString = f"INSERT OR IGNORE INTO {TABLE_NAME} (attribute_name, attribute_value, data_type) VALUES ('"+type+"_dateonly_" + countrycode + "_" + str(i) + "', '" + str(value) + "', '"+type+"')"
            c.execute(fString)
            connection.commit()
    except Error as e:
        print(e)


def add_to_database(url, n, inputdatatype):
    connection = create_connection(url)
    
    # Dictionary mapping inputdatatype to function
    function_mapping = {
        "numerical": add_numerical_data,
        "categorical": add_categorical_data,
        "categorical_licenseplate": add_categorical_licenseplate_data,
        "spatial_address": add_spatial_data,
        "spatial_geo": add_geo_data,
        "temporal": add_temporal_data,
        "datetime": add_temporal_data,
        "date": add_temporal_only_date_data,
        "time": add_temporal_only_time_data
    }

    # Get the function from the dictionary
    func = function_mapping.get(inputdatatype)

    # If the function exists, call it
    if func:
        for locale in LOCALES:
            fake = Faker(locale)
            func(connection, fake=fake, countrycode=locale, n=n)
    else:
        print("Invalid input data type")

    connection.close()

def addData(connection, fake : Faker, countrycode : str, n : int):
    items_per_category = math.ceil(n / 10)

    if items_per_category < 1:
        raise ValueError("Invalid number of items per category")
    
    # multipliers are evening out the number of items per category
    add_numerical_data(connection, fake = fake, countrycode = countrycode, n = items_per_category * 3 * len(LOCALES))
    add_categorical_data(connection, fake = fake, countrycode = countrycode, n = items_per_category * 2)
    add_categorical_licenseplate_data(connection, fake = fake, countrycode = countrycode, n = items_per_category)
    add_spatial_data(connection, fake = fake, countrycode = countrycode, n = items_per_category)
    add_geo_data(connection, fake = fake, countrycode = countrycode, n = items_per_category * 2)
    add_temporal_data(connection, fake = fake, countrycode = countrycode, n = items_per_category)
    add_temporal_only_date_data(connection, fake = fake, countrycode = countrycode, n = items_per_category)
    add_temporal_only_time_data(connection, fake = fake, countrycode = countrycode, n = items_per_category)


def read_database(url):
    connection = create_connection(url)
    read_data(connection)
    connection.close()

def read_data(connection):
    # MARK: - Read data
    try:
        c = connection.cursor()
        c.execute(f"SELECT * FROM {TABLE_NAME}")
        rows = c.fetchall()
        for row in rows:
            print(row)

        # Count rows per data type
        data_types = ['temporal', 'numerical', 'spatial']
        for data_type in data_types:
            c.execute(f"SELECT COUNT(*) FROM {TABLE_NAME} WHERE data_type = ?", (data_type,))
            count = c.fetchone()[0]
            print(f"Number of rows with data type '{data_type}': {count}")
    except Error as e:
        print(e)



def create_database(url, n):
    # usage
    connection = create_connection(url)
    create_table(connection)

    # Calculate number of items per locale and adds 15% more items to account for duplicates
    items_per_locale = math.ceil((n / len(LOCALES)) * 1.15) 

    if items_per_locale < 1:
        raise ValueError("Invalid number of items per locale")
    
    # MARK: - Add data
    for locale in LOCALES:
        fake = Faker(locale)
        addData(connection, fake, locale, items_per_locale)

    

    read_data(connection)

    connection.close();

# ---- CLI ----
# The functions defined in this section are wrappers around the main Python
# API allowing them to be called directly from the terminal as a CLI
# executable/script.


def parse_args(args):
    """Parse command line parameters

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--help"]``).

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Database creator")
    parser.add_argument(
        "--version",
        action="version",
        version=f"GraphPolaris_DataTypeInference {__version__}",
    )
    parser.add_argument(dest="url", help="URL to Database", type=str)
    parser.add_argument(dest="n", help="Number of items per category", type=int, metavar="INT")
    # parser.add_argument(dest="url", help="URL to Database", type=str, metavar="URL")

    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        help="set loglevel to INFO",
        action="store_const",
        const=logging.INFO,
    )
    parser.add_argument(
        "-vv",
        "--very-verbose",
        dest="loglevel",
        help="set loglevel to DEBUG",
        action="store_const",
        const=logging.DEBUG,
    )
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )



def reset_db(url):
    """
    Resets the database by dropping the table if it exists.

    Args:
        url (str): The URL of the database.

    Returns:
        None
    """
    try:
        connection = create_connection(url)
        c = connection.cursor()
        c.execute(f"DROP TABLE IF EXISTS {TABLE_NAME}")
        connection.commit()
        c.execute("DROP INDEX IF EXISTS idx_unique_data_type_inference")  # Delete the index
        connection.commit()
        connection.close()
    except Exception as e:
        print(f"An error occurred: {e}")


def main(args):
    """Wrapper allowing :func:`fib` to be called with string arguments in a CLI fashion

    Instead of returning the value from :func:`fib`, it prints the result to the
    ``stdout`` in a nicely formatted message.

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--verbose", "42"]``).
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.debug("Starting crazy calculations...")

    create_database(args.url, args.n)
    
    _logger.info("Script ends here")


def run():
    """Calls :func:`main` passing the CLI arguments extracted from :obj:`sys.argv`

    This function can be used as entry point to create console scripts with setuptools.
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m graphpolaris_datatypeinference.databasecreator 
    #
    run()
