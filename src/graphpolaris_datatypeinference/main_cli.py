"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
``[options.entry_points]`` section in ``setup.cfg``::

    console_scripts =
         fibonacci = graphpolaris_datatypeinference.skeleton:run

Then run ``pip install .`` (or ``pip install -e .`` for editable mode)
which will install the command ``fibonacci`` inside your current environment.

Besides console scripts, the header (i.e. until ``_logger``...) of this file can
also be used as template for Python modules.

Note:
    This file can be renamed depending on your needs or safely removed if not needed.

References:
    - https://setuptools.pypa.io/en/latest/userguide/entry_point.html
    - https://pip.pypa.io/en/stable/reference/pip_install
"""

import click
import logging
import sys

import constants
import databasecreator
import classifier_trainer

from graphpolaris_datatypeinference import __version__

__author__ = "Michael Behrisch"
__copyright__ = "Michael Behrisch"
__license__ = "MIT"

_logger = logging.getLogger(__name__)


# ---- Python API ----
# The functions defined in this section can be imported by users in their
# Python scripts/interactive interpreter, e.g. via
# `from graphpolaris_datatypeinference.skeleton import fib`,
# when using this Python module as a library.


@click.group
def mycommands():
    pass




def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )

@click.command()
@click.option("--url", default="filetypeinference.db", prompt="Enter the URL of the database: ", help="The URL of the database.")
def reset_db(url):
    databasecreator.reset_db(url)


@click.command()
@click.option("--loglevel", default="INFO", help="The log level to use.")
@click.option("--url", default="filetypeinference.db", prompt="Enter the URL of the database: ", help="The URL of the database.")
@click.option("--number", "-n", default=1000, prompt="Enter the number items to add to the database: ", help="The items that are added to the database per category.")
def create_database(loglevel, url, number):
    setup_logging(loglevel)
    databasecreator.create_database(url, number)

@click.command()
@click.argument("inputdatatype", type=click.Choice(constants.INPUT_DATA_TYPES))
@click.option("--loglevel", default="INFO", help="The log level to use.")
@click.option("--url", default="filetypeinference.db", prompt="Enter the URL of the database: ", help="The URL of the database.")
@click.option("--number", default=1000, prompt="Enter the number items to add to the database: ", help="The items that are added to the database per category.")
def add_to_database(loglevel, url, number, inputdatatype):
    setup_logging(loglevel)
    databasecreator.add_to_database(url, number, inputdatatype)

@click.command()
@click.option("--loglevel", default="INFO", help="The log level to use.")
@click.option("--url", default="filetypeinference.db", prompt="Enter the URL of the database: ", help="The URL of the database.")
def train_classifier(loglevel, url):
    setup_logging(loglevel)
    classifier_trainer.train_classifier(url)


@click.command()
@click.option("--loglevel", default="INFO", help="The log level to use.")
@click.option("--url", default="filetypeinference.db", prompt="Enter the URL of the database: ", help="The URL of the database.")
def read_database(loglevel, url):
    setup_logging(loglevel)
    databasecreator.read_database(url)

mycommands.add_command(create_database)
mycommands.add_command(reset_db)
mycommands.add_command(train_classifier)
mycommands.add_command(add_to_database)
mycommands.add_command(read_database)

if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m graphpolaris_datatypeinference.skeleton 42
    mycommands()
